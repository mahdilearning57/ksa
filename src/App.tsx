import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavbarComp from './components/Navbar/NavbarComp';

function App() {
  return (
    <div className="App">
     <NavbarComp/>
    </div>
  );
}

export default App;
