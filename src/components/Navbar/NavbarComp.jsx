import React from 'react'
import './NavbarComp.css'

export default function NavbarComp() {
  return (
    <>

      <div className='container'>
        <ul>
            <div className='right'>
              {/* <!-- LOGO --> */}
              <div className="logo">
                <img src='https://www.jed.gov.sa/themes/drupal8_zymphonies_theme/images/Logo.png'
                    alt=''
                    width="270"
                    height="90"
                    className="align-top"
                />
              </div>
            </div>
          </ul>
        <div className="nav-container">
        <div className='top-nav'>

          <div className="col-lg-6">
            <div className='left'>
              <div className="top-social">
                  
                  <ul>
                    <li>
                      <a href='#'>
                      <i className="fa fa-rss"></i>
                        موجز الأخبار
                      </a>
                    </li>
                    <li>
                      <a href='#'>
                        <i className="fa fa-phone"></i>
                          إتصل بنا
                      </a>
                    </li>
                    
                    <li>
                      <a href='#'>
                        <i className="fa fa-envelope"></i>
                        بريد الموظفين

                      </a>
                    </li>
                    <li>
                      <a href='#'>
                        <i className="fa fa-search"></i>
                      </a>
                    </li>
                  </ul>
                  
              </div>
            </div>
          </div>

          </div>
          <nav className="navbar">

          {/* <!-- NAVIGATION MENU --> */}
          <ul className="nav-links">
            {/* <!-- USING CHECKBOX HACK --> */}
            <input type="checkbox" id="checkbox_toggle" />
            <label htmlFor="checkbox_toggle" className="hamburger">&#9776;</label>
            {/* <!-- NAVIGATION MENUS --> */}
            <div className="menu">
              <li><a href="/">الهيكل التنظيمي</a></li>

              <li><a href="/">دليل الإجراءات</a></li>

              <li><a href="/">الخدمات الإلكترونية</a></li>

              <li><a href="/">عن جدة</a></li>

              <li><a href="/">كلمة المحافظ</a></li>

              <li><a href="/">الأخبار</a></li>

              <li><a href="/" className='active'>الرئيسية</a></li>

            </div>
          </ul>

          </nav> 
        </div>
        
      </div>

      {/* <div className='banner-area'>
          <div className="banner-content">
          <div className="social">
              <a href='#'>
              <i className="fa fa-envelope"></i>
              </a> 
              <a href='#'>
              <i className="fa fa-twitter"></i>
              </a> 
              <a href='#'>
              <i className="fa fa-youtube"></i>
              </a>
            </div> 
            <div className="other">
              <a href='#'>
                <i></i>
              </a>
            
              <a href='#'>
              <i className=""></i>
                الخدمات الإلكترونية
              </a>
              <a href='#'>
                <i className="fa fa-file-text-o"></i>
                الإستعلام الإلكتروني
              </a>
              <a href='#'>
                <i className=""></i>
                دليل الإجراءات
              </a> 
            </div>
          </div>
      </div>
      <div className='content-area'>
        <div className="wrapper">
        </div>
      </div> */}

      
    </>
  )
}
